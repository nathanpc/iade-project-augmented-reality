# TechView

A AR project to help regular people learn the world of technology.


## License

This project is licensed under the **MIT License**.


#### Disclaimer

I've been using Git in this project just as a way to go back to a previous state if things break and to have a history of my work. My Git skills are a lot better than what you see here.

For more of my work, please check out [my GitHub page](https://github.com/nathanpc).

