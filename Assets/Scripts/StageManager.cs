﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class StageManager : MonoBehaviour {
    public ContentPositioningBehaviour contentPositioning;
    public GameObject midAirPositioner;

    public void Start() {
        SetPositionerActive(false);
    }

    /**
     * Selects a stage to add to the AR experience.
     * 
     * @param stagePrefab Stage Prefab object to be used in the screen.
     */
    public void SelectStage(GameObject stagePrefab) {
        contentPositioning.AnchorStage = stagePrefab.GetComponent<AnchorBehaviour>();
        SetPositionerActive(true);
    }

    /**
     * Sets the state of the positioner.
     * 
     * @param active Should it be active?
     */
    public void SetPositionerActive(bool active) {
        midAirPositioner.SetActive(active);
    }
}
