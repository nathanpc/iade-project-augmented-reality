﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExplanationManager : MonoBehaviour {
	public Text nameTextBox;
	public Text explanationTextBox;
	public GameObject explanationScrollView;
	public GameObject midAirPositioner;

	/**
	 * Sets the component name text box.
	 * 
	 * @param name Component name.
	 */
	public void SetName(string name) {
		nameTextBox.text = name;
	}

	/**
	 * Sets the component explanation text box.
	 * 
	 * @param explanation Component explanation text file.
	 */
	public void SetExplanation(TextAsset explanation) {
		explanationTextBox.text = explanation.text;
	}

	/**
	 * Toggles the visibility of the explanation text box.
	 */
	public void ToggleExplanation() {
		explanationScrollView.SetActive(!explanationScrollView.activeSelf);
		midAirPositioner.SetActive(!explanationScrollView.activeSelf);
	}
}
