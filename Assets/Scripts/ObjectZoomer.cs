﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class ObjectZoomer : MonoBehaviour {
	private float minimumScale;
	public float zoomSpeed;

	private void Start() {
		// Set the minimum scale to the object's start scale.
		minimumScale = transform.localScale.x;
	}

	private void Update() {
		// Touched two points on screen, looks like a pinch to me.
		if (Input.touchCount == 2) {
			// Get the magnitude of the zooming action.
			float mag = GetZoomMagnitude();
			float factor = mag * zoomSpeed;

			// Actually scale the object respecting the minimum scale size.
			Vector3 newScale = transform.localScale + new Vector3(factor, factor, factor);
			if (newScale.x >= minimumScale) {
				transform.localScale = newScale;
			}
		}
	}

	public float GetZoomMagnitude() {
		// Store both touches.
		Touch touchZero = Input.GetTouch(0);
		Touch touchOne = Input.GetTouch(1);

		// Find the position in the previous frame of each touch.
		Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
		Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

		// Find the magnitude of the vector (the distance) between the touches in each frame.
		float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
		float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

		// Find the difference in the distances between each frame.
		return (prevTouchDeltaMag - touchDeltaMag) * -1;
	}
}
