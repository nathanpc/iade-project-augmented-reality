﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComponentManager : MonoBehaviour {
	public ScreenManager screenManager;
	public StageManager stageManager;
	public ExplanationManager explanationManager;
	public GameObject componentInfoScreen;

	/**
	 * Selects a component based on its name.
	 * 
	 * @param componentName Name of the component to be selected.
	 */
	public void SelectComponent(string componentName) {
		// Go to the information screen.
		screenManager.GoToScreen(componentInfoScreen);

		// Get the explanation and set some text boxes.
		TextAsset explanation = (TextAsset)Resources.Load(componentName, typeof(TextAsset));
		explanationManager.SetName(componentName);
		explanationManager.SetExplanation(explanation);
	}
}
