﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScreenManager : MonoBehaviour {
	public string mainMenuScene;
	public string howToScene;
	public List<GameObject> history;
	public GameObject topLevelScreen;
	public GameObject currentScreen;

	private void Start() {
		history = new List<GameObject>();
	}

	/**
	 * Goes to a scene and discards this one.
	 */
	public void GoToScene(string sceneName) {
		SceneManager.LoadScene(sceneName);
	}

	/**
	 * Goes to a scene and discards this one with a nice How To in the middle.
	 * 
	 * @param sceneName Name of the next scene.
	 */
	public void GoToSceneWithHowTo(string sceneName) {
		if (PlayerPrefs.GetInt("IgnoreHowTo", 0) == 1) {
			SceneManager.LoadScene(sceneName);
		} else {
			SceneManager.LoadScene(howToScene);
		}
	}

	/**
	 * Goes back to the main scene and discards this one.
	 */
	public void BackToMainScene() {
		SceneManager.LoadScene(mainMenuScene);
	}

	/**
	 * Goes to a specific screen.
	 * 
	 * @param screen Screen object to go to.
	 */
	public void GoToScreen(GameObject screen) {
		// Hide the current screen.
		currentScreen.SetActive(false);

		// Push the new screen into the history and set it as current.
		history.Add(screen);
		currentScreen = screen;

		// Show the new screen.
		currentScreen.SetActive(true);
	}

	/**
	 * Goes back to the previous screen.
	 */
	public void BackScreen() {
		// Check if we are already at the top level.
		if (history.Count == 0)
			return;

		// Hide the current screen.
		currentScreen.SetActive(false);

		// Go back in history.
		if (history.Count == 1) {
			// Go to the top level.
			history.Clear();
			currentScreen = topLevelScreen;
		} else {
			// Go back to an intermediate level.
			history.RemoveAt(history.Count - 1);
			currentScreen = history[history.Count - 1];
		}

		// Show the new screen.
		currentScreen.SetActive(true);
	}

	/**
	 * Set the ignore flag for the How To screen.
	 */
	public void SetHowToIgnore() {
		PlayerPrefs.SetInt("IgnoreHowTo", 1);
	}
}
