﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ObjectRotator : MonoBehaviour {
	private bool usedMouse;
	public float rotationSpeed;

	private void OnMouseDrag() {
		// Get rotation position.
		Vector2 pos = GetPointerPosition();

		// Normalize the rotation speed.
		float speed = NormalizeSpeed();

		// Rotate the object.
		RotateObject(pos, speed);
	}

	public void RotateObject(Vector2 pos, float speed) {
		transform.Rotate(
			(pos.y * speed * Time.deltaTime),
			(pos.x * speed * Time.deltaTime * -1),
			0,
			Space.World);
	}

	public float NormalizeSpeed() {
		if (usedMouse) {
			return rotationSpeed * 100;
		}

		return rotationSpeed;
	}

	public Vector2 GetPointerPosition() {
		// Get mouse input.
		usedMouse = true;
		float x = Input.GetAxis("Mouse X");
		float y = Input.GetAxis("Mouse Y");

		// Get touch input.
		if (Input.touchCount > 0) {
			usedMouse = false;
			x = Input.touches[0].deltaPosition.x;
			y = Input.touches[0].deltaPosition.y;
		}

		return new Vector2(x, y);
	}
}
